import { BrowserModule } from '@angular/platform-browser';
import { ChatModule } from './components/chat/chat.module';
import { NgModule } from '@angular/core';
import { ServiceModule } from './services/service.module';

import { AppComponent } from './app.component';
import { ExampleComponent } from './components/example/example.component';


@NgModule({
  declarations: [
    AppComponent,
    ExampleComponent,
  ],
  imports: [
    BrowserModule,
    ServiceModule,
    ChatModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
