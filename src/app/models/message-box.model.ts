export class MessageBox {
    constructor(
        public id: number,
        public user: string,
        public message: string,
        public time: Date,
    ) {}
}