import { Injectable } from '@angular/core';
import { MessageBox } from '../models/message-box.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  messageTray: MessageBox[];
  constructor() {
    this.messageTray = [];
  }

  _sendMessage(messageBox: MessageBox) {
    this.messageTray.push(messageBox);
    console.log(this.messageTray);
  }
}
