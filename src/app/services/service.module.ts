import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatService } from './chat.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ChatService
  ]
})
export class ServiceModule { }
