import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-chat',
  templateUrl: './button-chat.component.html',
  styleUrls: ['../chat.component.css']
})
export class ButtonChatComponent implements OnInit {

  @Input() nameButtonClass: string;
  @Output() showBoxChat: EventEmitter<boolean>;

  value: boolean;

  constructor() {
    this.value = true;
    this.showBoxChat = new EventEmitter();
  }

  ngOnInit() {
  }

  clickButton() {
    this.value = !this.value;
    this.showBoxChat.emit(this.value);
  }

}
