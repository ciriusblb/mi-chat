import { ChatService } from './../../../services/chat.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { MessageBox } from 'src/app/models/message-box.model';

@Component({
  selector: 'app-footer-chat',
  templateUrl: './footer-chat.component.html',
  styles: []
})
export class FooterChatComponent implements OnInit {
  message: string;
  messageBox: MessageBox;
  @Input() usuario: string;
  constructor(
    public chatServive: ChatService
  ) { }

  ngOnInit() {
  }
  sendMessage(form: NgForm) {
    if (form.invalid) {
      return;
    }
    const id = Math.floor(Math.random() * (100 - 0) + 0);
    this.messageBox = new MessageBox(id, this.usuario , form.value.message, new Date());
    this.chatServive._sendMessage(this.messageBox);
  }
}
