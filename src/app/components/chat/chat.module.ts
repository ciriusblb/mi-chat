import { MessageChatComponent } from './message-chat/message-chat.component';
import { FormsModule } from '@angular/forms';
import { ButtonChatComponent } from './button-chat/button-chat.component';
import { ChatComponent } from './chat.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderChatComponent } from './header-chat/header-chat.component';
import { FooterChatComponent } from './footer-chat/footer-chat.component';
import { BoxChatComponent } from './box-chat/box-chat.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    HeaderChatComponent,
    FooterChatComponent,
    BoxChatComponent,
    ButtonChatComponent,
    MessageChatComponent,
    ChatComponent
  ],
  exports: [
    ChatComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
  ]
})
export class ChatModule { }
