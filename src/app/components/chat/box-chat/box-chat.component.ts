import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from './../../../services/chat.service';


@Component({
  selector: 'app-box-chat',
  templateUrl: './box-chat.component.html',
  styleUrls: ['../chat.component.css']
})
export class BoxChatComponent implements OnInit {

  @Input() nameBoxClass: string;
  boxValue: boolean;
  @Input() usuario: string;
  constructor(
    public chatServive: ChatService
  ) {
  }
  ngOnInit() {
  }

  @Input()
  set showBoxValue(boxValue: boolean) {
    this.boxValue = boxValue;
  }
}
