import { Component, OnInit, Input } from '@angular/core';
import { MessageBox } from 'src/app/models/message-box.model';

@Component({
  selector: 'app-message-chat',
  templateUrl: './message-chat.component.html',
  styleUrls: ['../chat.component.css']
})
export class MessageChatComponent implements OnInit {

  MessageBox: MessageBox;
  constructor() { }

  ngOnInit() {
  }

  @Input()
  set showMessageBox(messageBox: MessageBox) {
    this.MessageBox = messageBox;
  }

}
