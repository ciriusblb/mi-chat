import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  styleClass: any;
  showBox: any;

  constructor() {
    this.styleClass = {
      ULC: 'UpperLeftCorner',
      URC: 'UpperRightCorner',
      LLC: 'LowerLeftCorner',
      LRC: 'LowerRightCorner'
    };
    this.showBox = {
      box1: true,
      box2: true,
      box3: true,
      box4: true
    };
  }

  ngOnInit() {
  }
}
